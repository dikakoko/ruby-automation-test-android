FROM ruby:3.0.0-alpine

RUN apk update && apk add --no-cache --virtual build-dependencies build-base jq bash curl zip tzdata wget

# When optimizing the size of an Alpine image, the runtime libraries must be permanently added.
# Additionally, adding and removing development tooling can be chained with gem installation to ensure a small layer.
RUN apk add --no-cache libxml2 libxslt && \
        apk add --no-cache --virtual .gem-installdeps build-base libxml2-dev libxslt-dev && \
        gem install nokogiri --platform=arm64-darwin-22 -- --use-system-libraries && \
        rm -rf $GEM_HOME/cache && \
        apk del .gem-installdeps

# Run install Chrome
RUN apk add chromium chromium-chromedriver && \
        rm -rf /var/cache/apk/*

WORKDIR /app

COPY Gemfile Gemfile.lock ./

# Install bundler with version 2.4.20
RUN gem install bundler:2.4.20

# Install all ruby gems
RUN bundle install

COPY . .

CMD ["bash"]
